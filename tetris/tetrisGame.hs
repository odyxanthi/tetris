import System.IO
import Tetris
import Control.Applicative
import Control.Monad
import System.Random

import Graphics.Gloss
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Data.ViewPort
import Graphics.Gloss.Data.ViewState
import Graphics.Gloss.Interface.Pure.Game

type Scene = GameState

mainWndWidth = 800
mainWndHeight = 600

squareSideInPixels :: Float
squareSideInPixels = 20

--gameWndRows :: Int
gameWndRows = 35
gameWndCols = 18



data Rect a = Rect {
     top :: a
    ,bottom :: a
    ,left :: a
    ,right :: a
} deriving (Eq, Show)


rectHeight :: (Num a) => Rect a -> a
rectHeight rect = (top rect) - (bottom rect)

rectWidth :: (Num a) => Rect a -> a
rectWidth rect = (right rect) - (left rect)

createRect_LeftTop :: Num a => a -> a -> a -> a -> Rect a
createRect_LeftTop left top width height = 
    Rect {
        top = top
       ,bottom = b
       ,left = left
       ,right = r
    }
    where r = left+width
          b = top - height

          
createRect_LeftBottom :: Num a => a -> a -> a -> a -> Rect a
createRect_LeftBottom left bottom width height = 
    Rect {
        top = top
       ,bottom = bottom
       ,left = left
       ,right = r
    }
    where r = left+width
          top = bottom + height          



gameWndRect :: Rect Float 
gameWndRect = 
    let w = squareSideInPixels * (fromIntegral gameWndCols)
        h = squareSideInPixels * (fromIntegral gameWndRows)
        l = (-mainWndWidth * 0.5) + 100
        b = (-mainWndHeight * 0.5) + 20
    in createRect_LeftBottom l b w h


horizontalMargin = 50
blockedPositionColor = green
fallingTileColor = red
guiBgColor = blue 


drawBlock :: (Int,Int) -> Picture        
drawBlock (row,col) = drawRect $ createRect_LeftBottom l b w h
    where   l = (fromIntegral col) * squareSideInPixels
            b = (fromIntegral row) * squareSideInPixels
            w = squareSideInPixels
            h = squareSideInPixels


drawRect :: Rect Float -> Picture
drawRect rect = 
    Polygon [(l,t),(r,t),(r,b),(l,b)]
    where l = (left rect ) :: Float
          r = (right rect ) :: Float
          t = (top rect ) :: Float
          b = (bottom rect ) :: Float


drawGameState :: Tetris.GameState -> Picture
drawGameState gameState = 
    pictures [color blockedPositionColor blockedPosPict, 
              color fallingTileColor currentTilePict]
    where blockedPosPict = pictures $ map drawPos2D (blockedPositions gameState) 
          currentTilePict = pictures $ map drawPos2D (blocks $ fallingTile gameState)  
          drawPos2D = drawBlock . posToTuple 
          posToTuple p = (row p, col p)


drawGameWnd :: Scene -> Picture
drawGameWnd sc = 
    pictures [backgroundRect, content]
    where backgroundRect = color (greyN 0.5) $ drawRect gameWndRect
          content = translate l b $ drawGameState sc
          l = left gameWndRect
          b = bottom gameWndRect
    
    
draw2D :: Scene -> Picture
draw2D sc = 
    -- pictures [ color red $ drawRect 10 100 10 100,
               -- color orange $ drawRect (-100) 100 10 100,
               -- color green $ drawRect 100 10 100 10,
               -- color yellow $ drawRect 100 110 100 10,
               -- color white $ drawRect 0 0 10 10
             -- ]
    pictures [drawGameWnd sc] --, drawInfoWnd, drawNextTileWnd]
          
          
drawScene :: Scene -> Picture
drawScene = drawGameState          

eventToUiCode :: Event -> Maybe UserInput
eventToUiCode e = case e of
    EventKey (Char 'A')            Down _ _ -> Just ROTATE_CCW
    EventKey (Char 'S')            Down _ _ -> Just ROTATE_CW
    EventKey (Char 'a')            Down _ _ -> Just ROTATE_CCW
    EventKey (Char 's')            Down _ _ -> Just ROTATE_CW
    EventKey (SpecialKey KeyLeft)  Down _ _ -> Just MOVE_LEFT
    EventKey (SpecialKey KeyRight) Down _ _ -> Just MOVE_RIGHT
    EventKey (SpecialKey KeyDown)  Down _ _ -> Just MOVE_DOWN
    _ -> Nothing

    
onTimerUpdate :: Float -> Scene -> Scene
onTimerUpdate dt sc
    | timeSinceLastUpdate > 0.3 = 
        let sc' = updateState_CycleCompleted sc
        in sc' { timeEllapsed = 0.0 }
    | otherwise = 
        sc { timeEllapsed = timeSinceLastUpdate }
    where timeSinceLastUpdate = dt + (timeEllapsed sc)



handleEvent :: Event -> Scene -> Scene
handleEvent e sc =
    case uiCode of Nothing -> sc
                   Just ui -> updateState_UserInput ui sc
    where uiCode = eventToUiCode e
                
--handleEvent e sc = sc { fallingTile = moveTileLeft $ fallingTile sc }

-- handleEvent :: Event -> Scene -> Scene
-- handleEvent (EventKey (MouseButton LeftButton) Down Modifiers{shift = Down} pos) sc 
 -- = case findVertex (invertViewPort port pos) (viewPortScale port) sc of
        -- Nothing -> sc
        -- Just v  -> sc{scSelected = Just v}
   -- where viewState = scViewState sc
         -- port      = viewStateViewPort viewState


windowSize :: (Int, Int)
windowSize = (800, 600)         


standardTiles :: [Tile]
standardTiles = [t1,t2,t3,t4,t5,t6,t7,t8,t9]
    where 
        t1 = Tile {                                                --    #
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 0 1, Pos2D 0 2]  --    #
           ,rotationPt = Pos2D 0 0                                 --    ##
        }
        t2 = Tile {
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 1 1, Pos2D 1 2]  --    #
           ,rotationPt = Pos2D 0 0                                 --    #
        }                                                          --   ## 
        t3 = Tile {
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 0 1, Pos2D 1 1]  --   ##
           ,rotationPt = Pos2D 0 0                                 --   ##
        }
        t4 = Tile {
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 1 1, Pos2D 2 1]  --    ##
           ,rotationPt = Pos2D 0 0                                 --   ##
        }
        t5 = Tile {
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 2 0, Pos2D 3 0]  --   ####
           ,rotationPt = Pos2D 0 0
        }
        t6 = Tile {                                                --    #
            blocks = [Pos2D 0 0, Pos2D 1 0, Pos2D 2 0, Pos2D 1 1]  --   ###
           ,rotationPt = Pos2D 1 0
        }
        t7 = Tile {                                                --    # #
            blocks = [Pos2D 0 0, Pos2D 1 1, Pos2D 2 0, Pos2D 3 1]  --   # #
           ,rotationPt = Pos2D 1 0
        }
        t8 = Tile {                                                --    # 
            blocks = [Pos2D 0 1, Pos2D 2 1, Pos2D 1 2, Pos2D 1 0]  --   # #
           ,rotationPt = Pos2D 1 0                                 --    #
        }
        t9 = Tile {                                                --    # 
            blocks = [Pos2D 0 0, Pos2D 1 1, Pos2D 2 2, Pos2D 3 3]  --   # 
           ,rotationPt = Pos2D 1 0                                 --  #  
        }                                                          -- #
        
        

main = do  
    let t1 = Tile {
        blocks = [(Pos2D 5 5),(Pos2D 6 6),(Pos2D 7 6)]
       ,rotationPt = Pos2D 5 5
    }

    g <- newStdGen
    let gameInitState = Tetris.GameState {
        gameWndSize = GameWndSize gameWndRows gameWndCols
       ,tileGenerator = TileGenerator g standardTiles
       ,blockedPositions = [(Pos2D 1 0), (Pos2D 2 0), (Pos2D 3 0) ]
       ,fallingTile = t1
       ,nextTile = t1
       ,timeEllapsed = 0
    }
    
    print $ show gameInitState

    play (InWindow "my tetris ..." windowSize (10, 10))
        guiBgColor 30 gameInitState draw2D handleEvent onTimerUpdate
    
    -- animate (InWindow "Tetris" (500, 650) (20,  20))
             -- black (\_ -> pictures [drawGameState gameInitState])



