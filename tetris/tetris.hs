module Tetris
(
GameWndSize(GameWndSize),
    rows,
    cols,
Pos2D(Pos2D),
    row,
    col,
Tile(Tile),
    blocks,
    rotationPt,
TileGenerator(TileGenerator),
    randomNumGen,
    possibleTiles,

GameState(GameState),
    gameWndSize,
    tileGenerator,
    blockedPositions,
    fallingTile,
    nextTile,
    timeEllapsed,
UserInput(MOVE_DOWN,MOVE_LEFT,MOVE_RIGHT,ROTATE_CW,ROTATE_CCW),
    
-- general    
    moveTileDown,
    moveTileLeft,
    
    updateState_CycleCompleted,
    updateState_UserInput
)
where

import Data.List
import Data.Bool
import System.Random


data Pos2D = Pos2D {
    row :: Int
   ,col :: Int
} deriving (Show,Eq)


data GameWndSize = GameWndSize {
    rows :: Int
   ,cols :: Int
} deriving (Eq,Show)


data Tile = Tile {
    blocks :: [Pos2D]
   ,rotationPt :: Pos2D
} deriving (Show,Eq)


data TileGenerator = TileGenerator {
    randomNumGen :: StdGen
   ,possibleTiles :: [Tile]
}

instance Show TileGenerator where
    show tileGenerator = show $ possibleTiles tileGenerator

    

data GameState = GameState {
    gameWndSize :: GameWndSize
   ,tileGenerator :: TileGenerator
   ,blockedPositions :: [Pos2D]
   ,fallingTile :: Tile
   ,nextTile :: Tile
   ,timeEllapsed :: Float
} deriving (Show)





data GamePlayParams = GamePlayParams {
    keyboardSamplePerSec :: Int
   ,timerTicksPerSec :: Int
}


data UserInput = MOVE_DOWN | MOVE_LEFT | MOVE_RIGHT | ROTATE_CW | ROTATE_CCW deriving (Show,Eq)

type TileTransform = (Tile->Tile)


--------------------------------------------------------------------------------------
---------------------          Tile transforms         -------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

translate2D :: Pos2D -> (Int,Int) -> Pos2D            
translate2D (Pos2D row col) (dr,dc) = Pos2D (row+dr) (col+dc)


translateTile2D :: Tile -> (Int,Int) -> Tile
translateTile2D t (drow,dcol) = Tile blocks' rotationPt'
    where blocks' = map applyTranslation $ blocks t
          rotationPt' = applyTranslation $ rotationPt t
          applyTranslation pos2D = translate2D pos2D (drow,dcol) 

moveTileDown :: Tile -> Tile
moveTileDown t = translateTile2D t (-1,0)

moveTileUp :: Tile -> Tile
moveTileUp t = translateTile2D t (1,0)

moveTileLeft :: Tile -> Tile
moveTileLeft t = translateTile2D t (0,-1)

moveTileRight  :: Tile -> Tile
moveTileRight t = translateTile2D t (0,1)

rotateTileCW :: Tile -> Tile
rotateTileCW t = 
    t { blocks = map rotateCW $ blocks t }
    where
        rotateCW pt = Pos2D (r-dc) (c+dr)
            where 
                rotCenter = rotationPt t
                r = row rotCenter
                c = col rotCenter
                dr = row pt - r
                dc = col pt - c
         

rotateTileCCW :: Tile -> Tile
rotateTileCCW t = 
    t { blocks = map rotateCCW $ blocks t }
    where
        rotateCCW pt = Pos2D (r+dc) (c-dr)
            where 
                rotCenter = rotationPt t
                r = row rotCenter
                c = col rotCenter
                dr = row pt - r
                dc = col pt - c


-- test if tile overlaps given positions                
tileOverlapsPositions :: Tile -> [Pos2D] -> Bool
tileOverlapsPositions tile testPositions 
    | testPositions `overlap` (blocks tile) = False
    | otherwise = True
    where  overlap l1 l2 = not . null $ intersect l1 l2
    

tileOutOfBounds :: Tile -> GameWndSize -> Bool
tileOutOfBounds tile wndSize = 
    any blockOutOfBounds (blocks tile) 
        where blockOutOfBounds block = 
                (row block) < 0 || 
                (col block) < 0 || 
                (col block) >= (cols wndSize)

--------------------------------------------------------------------------------------
---------------------          Tile generation         -------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------                    
                
getRandomTile :: TileGenerator -> (Tile,TileGenerator)
getRandomTile tileGen = (tile,tileGen')
    where
        numTiles = length $ possibleTiles tileGen
        (tileIndex,gen) = randomR (0,numTiles-1) $ randomNumGen tileGen
        tile = (possibleTiles tileGen) !! tileIndex
        tileGen' = tileGen { randomNumGen = gen }


--------------------------------------------------------------------------------------
---------------------          Game state utilities    -------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------    

isRowFull :: Int -> GameState -> Bool
isRowFull rowIndex st =
    all positionBlocked positions
    where
        positionBlocked pos = pos `elem` (blockedPositions st)
        positions = [Pos2D rowIndex col | col <- [0 .. numColumns-1]]
        numColumns = cols $ gameWndSize st
    

collapseRow :: Int -> GameState -> GameState
collapseRow rowIndex st =
    st { blockedPositions = blockedAfterRowCollapse }
    where 
        blockedAfterRowCollapse = dropElemsAbove . filterRowElems $ blockedPositions st
        filterRowElems = filter (\pos -> row pos /= rowIndex)
        dropElemsAbove = map (\(Pos2D row col) -> if row > rowIndex then Pos2D (row-1) col else Pos2D row col)
        
        
collapseFullRows :: GameState -> GameState
collapseFullRows st = 
    foldl' collapseRow' st fullRowsIdxs
    where
        collapseRow' st i = collapseRow i st
        fullRowsIdxs = reverse . filter (\i -> isRowFull i st) $ rowIdxs 
        rowIdxs = [0.. numRows-1]
        numRows = rows $ gameWndSize st
    
--------------------------------------------------------------------------------------
---------------------          Game loop events        -------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------    

data IllegalMoveHandling = CancelMove | AddToBlocked

transformFallingTile :: IllegalMoveHandling -> TileTransform -> GameState -> GameState
transformFallingTile i t st =
    case i of CancelMove -> transformFallingTile_CancelIllegal t st
              AddToBlocked -> transformFallingTile_AddToBlocked t st
              

transformFallingTile_CancelIllegal :: TileTransform -> GameState -> GameState
transformFallingTile_CancelIllegal trans st =
    st { fallingTile = fallingTile' }
    where
        initTile = fallingTile st
        transformedTile = trans initTile
        
        moveLegal = (tileOutOfBounds transformedTile (gameWndSize st) == False) &&
                    (tileOverlapsPositions transformedTile (blockedPositions st))
     
        fallingTile' = case moveLegal of
                            True -> transformedTile
                            False -> initTile
        
                                 
                                 
transformFallingTile_AddToBlocked :: TileTransform -> GameState -> GameState
transformFallingTile_AddToBlocked trans st =
    st {
        fallingTile = fallingTile',
        blockedPositions = blockedPositions',
        tileGenerator = tileGenerator'
    }
    where
        
        initTile = fallingTile st
        transformedTile = trans initTile
        
        moveLegal = (tileOutOfBounds transformedTile (gameWndSize st) == False) &&
                    (tileOverlapsPositions transformedTile (blockedPositions st))
    
        (randomTile,tileGenTmp) = getRandomTile $ tileGenerator st
        tileGenerator' = case moveLegal of
                            True -> tileGenerator st
                            False -> tileGenTmp
        
        fallingTile' = case moveLegal of
                            True -> transformedTile
                            False -> translateTile2D randomTile (20,0)
        blockedPositions' = case moveLegal of
                                 True -> blockedPositions st
                                 False -> (blockedPositions st) ++ (blocks initTile)                                 

                        

                                 
                                 
    
updateState_CycleCompleted :: GameState -> GameState
updateState_CycleCompleted st = collapseFullRows . transformFallingTile AddToBlocked moveTileDown $ st

          

updateState_UserInput :: UserInput -> GameState -> GameState
updateState_UserInput ui st
    | ui == MOVE_DOWN   = collapseFullRows . transformFallingTile AddToBlocked moveTileDown $ st
    | ui == MOVE_LEFT   = collapseFullRows . transformFallingTile CancelMove moveTileLeft $ st
    | ui == MOVE_RIGHT  = collapseFullRows . transformFallingTile CancelMove moveTileRight $ st
    | ui == ROTATE_CW   = collapseFullRows . transformFallingTile CancelMove rotateTileCW $ st
    | ui == ROTATE_CCW  = collapseFullRows . transformFallingTile CancelMove rotateTileCCW $ st
    | otherwise = st